import asyncio
import functools


def background(f):
    async def wrapped(*args, **kwargs):
        return await asyncio.get_event_loop().run_in_executor(None, functools.partial(f, *args, **kwargs))

    return wrapped
