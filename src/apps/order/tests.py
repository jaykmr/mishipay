import json

from django.contrib.auth.models import User
# Create your tests here.
from rest_framework import status
from rest_framework.reverse import reverse_lazy, reverse
from rest_framework.test import APITestCase
from rest_framework_simplejwt.tokens import SlidingToken

from apps.order.models import Order
from apps.order.serializers import OrderSerializer


# Create your tests here.


class OrderTestCase(APITestCase):
    url = reverse_lazy('place_order')

    def setUp(self):
        user = User.objects.create_user(username="test", password="test1234", email="test@check.com")
        access_token = str(SlidingToken.for_user(user))
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + access_token)

    def test_place_order_success(self):
        # get API response
        line_items = dict(order=dict(line_items=[
            dict(variant_id=32328256716867, quantity=2)
        ]))
        response = self.client.post(self.url, line_items, format='json')
        # get data from db
        order = Order.objects.all()[0]
        serializer = OrderSerializer(order)
        self.assertEqual(json.loads(response.content).get('order').get('id'), serializer.data.get('id'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_place_order_method_not_allowed(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
